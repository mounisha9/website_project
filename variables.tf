variable "region" {
  default     = "ap-south-1"
  type        = string
  description = "this is a mumbai region"
}
variable "instance_type" {
  type        = string
  description = "for practice purpose we are using t2.micro"
}
variable "ami" {
  type        = string
  description = "This is ubuntu ami id"
}
variable "key_name" {
  type        = string
  description = "This is a key_name of pem"
}
variable "private_key_path" {
  type        = string
  description = "this is the path of private_key"
}